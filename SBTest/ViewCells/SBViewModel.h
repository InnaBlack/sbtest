//
//  SBViewModel.h
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cat.h"

@interface SBViewModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *strImgUrl;
@property (nonatomic, strong) NSString *strImgUrlNew;

@property (nonatomic, strong) NSString *strHtml;

@property (nonatomic, strong) Cat *model;

-(instancetype) initWithModel:(Cat *) model;

@end
