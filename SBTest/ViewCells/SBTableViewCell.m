//
//  SBTableViewCell.m
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import "SBTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>


@implementation SBTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setViewModel:(SBViewModel *)viewModel{
    
    _viewModel = viewModel;
    
    if (_viewModel) {
        
        if (![viewModel.strImgUrlNew isEqualToString:@""]) {
            [self.catImage sd_setImageWithURL: [NSURL URLWithString:viewModel.strImgUrlNew]];
        }
        
    } else {
       
        self.catImage = nil;
    }
    
    
}


@end
