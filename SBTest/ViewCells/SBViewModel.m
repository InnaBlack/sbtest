//
//  SBViewModel.m
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import "SBViewModel.h"


@implementation SBViewModel

-(instancetype) initWithModel:(Cat *) modelCat {
    
    self = [super init];
    
    if (!self) {
        return nil;
    }
    _model = modelCat;
    _name = modelCat.name;
    _strImgUrl = modelCat.strImgUrl;
    _strHtml = modelCat.strHtml;
    _strImgUrlNew = @"";
  
    return self;
}

@end
