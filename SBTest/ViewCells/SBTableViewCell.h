//
//  SBTableViewCell.h
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBViewModel.h"


@interface SBTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *catImage;

@property (nonatomic, strong) SBViewModel *viewModel;

@end
