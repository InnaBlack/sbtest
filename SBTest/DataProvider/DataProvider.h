//
//  DataProvider.h
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataProviderDelegate <NSObject>

-(void) didLoadData:(id) data;
-(void) didLoadImageUrl:(NSString *)strUrl index: (NSIndexPath*) ind;
-(void) didGetImageError:(NSError *) error;
-(void) didGetError:(NSError *) error;

@end

@interface DataProvider : NSObject

@property (nonatomic, strong) id data;
@property (nonatomic, weak) id <DataProviderDelegate> delegete;

-(void) loadData;
-(void) loadImageStr: (NSString *) strOld index: (NSIndexPath*) ind;

@end
