//
//  APIClient.m
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import "APIClient.h"

@implementation APIClient


+(void) getCatComplection: (void(^) (NSArray* response))success falture:(void (^)(NSError* error)) falture {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^ {
        success([self getArrayResult]);
    });
    
}

+(NSArray *) getArrayResult {
    
    NSMutableArray *arr = [NSMutableArray new];
    
    for (int i=0; i<100; i++) {
        NSMutableDictionary *catDictionary = [NSMutableDictionary new];
        [catDictionary setValue:[NSString stringWithFormat:@"Name Cat%@", [self generateRandomString:100] ] forKey:@"name"];
        [catDictionary setValue:@"http://thecatapi.com/api/images/get?format=src&type=gif" forKey:@"strImgUrl"];
        [catDictionary setValue:@"http://loripsum.net/api" forKey:@"strHtml"];
        
        [arr addObject:catDictionary];
    }
    
    return arr;
    
}

+(NSString*)generateRandomString:(int)num {
    NSMutableString *string = [NSMutableString stringWithCapacity:num];
    for (int i = 0; i < num; i++) {
        [string appendFormat:@"%C", (unichar)('a' + arc4random_uniform(26))];
    }
    return string;
}

@end
