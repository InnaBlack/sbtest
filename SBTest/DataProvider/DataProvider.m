//
//  DataProvider.m
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import "DataProvider.h"
#import "APIClient.h"
#import "Cat.h"

@implementation DataProvider

-(instancetype) init {
    self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}

-(void) loadData {
    [APIClient getCatComplection:^(NSArray *response) {
        if ([response isKindOfClass:NSArray.class]) {
            [self didLoadData: [Cat catArrayFromArrayOfDictionary:response]];
        }
    } falture:^(NSError *error) {
        [self didGetError: error];
    }];
    
}

-(void) loadImageStr: (NSString *) strOld index: (NSIndexPath*) ind {
    NSURL *URL = [NSURL URLWithString:strOld];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    __weak DataProvider *weakSelf = self;
   
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               NSLog(@"%@", response.URL.absoluteString);
                               if (!error) {
                                   [weakSelf didLoadImageUrl:response.URL.absoluteString index:ind];
                               }else {
                                   [weakSelf didGetImageError:error];
                               }
                               
                           }];
    
}

-(void) didLoadData: (id) data {
    self.data = data;
    
    if ([self.delegete respondsToSelector:@selector(didLoadData:)]) {
        [self.delegete didLoadData:data];
    }
    
}


-(void) didLoadImageUrl:(NSString *)strUrl index: (NSIndexPath*) ind{
   
    if ([self.delegete respondsToSelector:@selector(didLoadImageUrl: index:)]) {
        [self.delegete didLoadImageUrl:strUrl index:ind];
    }
}

-(void) didGetImageError:(NSError *) error{
    
}

-(void) didGetError: (NSError *) error{
    if ([self.delegete respondsToSelector:@selector(didGetError:)]) {
        [self.delegete didGetError:error];
    }
}



@end
