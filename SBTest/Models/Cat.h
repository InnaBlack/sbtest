//
//  Cat.h
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cat : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *strImgUrl;
@property (nonatomic, strong) NSString *strHtml;

+(instancetype) catFromdictionary: (NSDictionary*) dictionary;
+(NSArray*) catArrayFromArrayOfDictionary: (NSArray<NSDictionary*>*) arrayOfDic;
    
@end
