//
//  Cat.m
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import "Cat.h"

@implementation Cat

+(instancetype) catFromdictionary: (NSDictionary*) dictionary{
    Cat *catModel = [Cat new];
    
    if ([dictionary isKindOfClass:NSDictionary.class]) {
        if (dictionary[@"name"] && [dictionary[@"name"] isKindOfClass: NSString.class]){
            catModel.name = dictionary[@"name"];
           
        }
    }
    
    
    if ([dictionary isKindOfClass:NSDictionary.class]) {
        if (dictionary[@"strImgUrl"] && [dictionary[@"strImgUrl"] isKindOfClass: NSString.class]){
            catModel.strImgUrl = dictionary[@"strImgUrl"];
        }
    }
    
    if ([dictionary isKindOfClass:NSDictionary.class]) {
        if (dictionary[@"strHtml"] && [dictionary[@"strHtml"] isKindOfClass: NSString.class]){
            NSURL *urlRequest = [NSURL URLWithString:dictionary[@"strHtml"]];
            NSError *err = nil;
            catModel.strHtml = [NSString stringWithContentsOfURL:urlRequest encoding:NSUTF8StringEncoding error:&err];
            
        }
    }
    
    return catModel;
}

+(NSArray*) catArrayFromArrayOfDictionary: (NSArray<NSDictionary*>*) arrayOfDic {
    NSMutableArray *arr = [NSMutableArray new];
    
    [arrayOfDic enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Cat *cat = [Cat catFromdictionary:obj];
        if (cat) {
            [arr addObject:cat];
        }
        
    }];
    
    return [arr copy];
}

@end
