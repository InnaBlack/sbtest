//
//  SBDetailsVC.m
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import "SBDetailsVC.h"

@interface SBDetailsVC ()

@end

@implementation SBDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *strImage =  [NSString stringWithFormat:@"<img style='width:200%; display:block; margin-left: auto; margin-right: auto;' src='%@'></img>", self.catDetail.strImgUrlNew];
    NSString *strDetail = [NSString stringWithFormat:@"%@%@",strImage, self.catDetail.strHtml];
    [self.catImage loadHTMLString:strDetail baseURL:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
