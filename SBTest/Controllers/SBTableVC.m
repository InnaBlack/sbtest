//
//  SBTableVC.m
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import "SBTableVC.h"
#import "SBTableViewCell.h"
#import "SBDetailsVC.h"
#import "DataProvider.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SBTableVC () <DataProviderDelegate>
@property(nonatomic, strong) DataProvider *dataProvider;

@end

@implementation SBTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Cats :)";
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SBTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SBTableViewCell"];
    
    self.dataProvider = [DataProvider new];
    self.dataProvider.delegete = self;
    [self.dataProvider loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - DataProviderDelegate

-(void)didLoadData:(id)data{
    NSMutableArray *viewModelArray = [NSMutableArray new];
   
    if ([data isKindOfClass:NSArray.class]) {
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
           [viewModelArray addObject: [[SBViewModel alloc] initWithModel:obj]];
        }];
        
    }
    self.viewModels = viewModelArray;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

-(void) didLoadImageUrl:(NSString *)strUrl index: (NSIndexPath*) ind{
    SBViewModel *model =  _viewModels[ind.row];
    model.strImgUrlNew = strUrl;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadRowsAtIndexPaths:@[ind] withRowAnimation:UITableViewRowAnimationAutomatic];
    });
}

-(void) didGetImageError:(NSError *)error {
    
}

- (void)didGetError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}



#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.viewModels.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SBViewModel *modelView = (SBViewModel*)self.viewModels[indexPath.row];
    
    SBTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SBTableViewCell" forIndexPath:indexPath];
    
    cell.viewModel = modelView;
    
    if ([cell.viewModel.strImgUrlNew isEqualToString:@""]) {
        [self.dataProvider loadImageStr:modelView.strImgUrl index:indexPath];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SBDetailsVC *sbDetail = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"SBDetailsVC"];
    sbDetail.catDetail = self.viewModels[indexPath.row];
    [[self navigationController] pushViewController:sbDetail animated:YES];
}


@end
