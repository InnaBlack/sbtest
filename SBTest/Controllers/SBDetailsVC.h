//
//  SBDetailsVC.h
//  SBTest
//
//  Created by  inna on 5/7/18.
//  Copyright © 2018  inna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBViewModel.h"

@interface SBDetailsVC : UIViewController

@property(nonatomic, strong) SBViewModel *catDetail;

@property (weak, nonatomic) IBOutlet UIWebView *catImage;

@end
